#include <stdio.h>
#include <string.h>

#define CANTIDAD_PERSONAS 10

#define TAMANO_TEXTO 16
#define TAMANO_COMPLETO 32

#define EDAD_MAX 130
#define EDAD_MIN 0

int validarTexto(char *);
int validarEdad(int);

int main()
{
	char nombre_ingresado[TAMANO_TEXTO];
	char apellido_ingresado[TAMANO_TEXTO];
	int edad_ingresada;

	int cantidad_personas = CANTIDAD_PERSONAS;
	int iterador_personas;

	int edad_sumatoria = 0;
	float edad_promedio = 0;

	char mayor_nombre[TAMANO_TEXTO];
	char mayor_apellido[TAMANO_TEXTO];
	int mayor_edad = EDAD_MIN-1;

	char menor_nombre[TAMANO_TEXTO];
	char menor_apellido[TAMANO_TEXTO];
	int menor_edad = EDAD_MAX+1;

	if(cantidad_personas<1) cantidad_personas = 10;

	printf("Ingrese la información de %i persona", cantidad_personas);
	if(cantidad_personas>1)
	{
		printf("s");
	}
	printf("\n");

	for(iterador_personas = 1; iterador_personas <= cantidad_personas; iterador_personas++)
	{
		printf("Persona #%i\n", iterador_personas);

		do
		{
			printf("\tIngrese su nombre:   ");
			scanf("%s", nombre_ingresado);

			if(validarTexto(nombre_ingresado))
			{
				break;
			}
			else
			{
				printf("\t\tEl nombre y apellido deben de empezar con mayúscula\n");
				printf("\t\ty el resto ser minúsculas.\n");
				printf("\t\tNo debe de tener números ni caracteres especiales.\n");
				printf("\t\tIngrese nuevamente la información solicitada.\n");
			}
		}while(1);

		do
		{
			printf("\tIngrese su apellido: ");
			scanf("%s", apellido_ingresado);

			if(validarTexto(apellido_ingresado))
			{
				break;
			}
			else
			{
				printf("\t\tEl nombre y apellido deben de empezar con mayúscula\n");
				printf("\t\ty el resto ser minúsculas.\n");
				printf("\t\tNo debe de tener números ni caracteres especiales.\n");
				printf("\t\tIngrese nuevamente la información solicitada.\n");
			}
		}while(1);

		do
		{
			printf("\tIngrese su edad:     ");
			scanf("%i", &edad_ingresada);

			if(validarEdad(edad_ingresada))
			{
				break;
			}
			else
			{
				printf("\t\tLa edad debe de ser un número entre 0 y 130.\n");
				printf("\t\tIngrese nuevamente la información solicitada.\n");
			}
		}while(1);

		edad_sumatoria += edad_ingresada;

		if(edad_ingresada < menor_edad)
		{
			//menor_nombre = nombre_ingresado;
			strncpy ( menor_nombre, nombre_ingresado, sizeof(menor_nombre) );
			//menor_apellido = apellido_ingresado;
			strncpy ( menor_apellido, apellido_ingresado, sizeof(menor_apellido) );
			menor_edad = edad_ingresada;
		}

		if(edad_ingresada > mayor_edad)
		{
			//mayor_nombre = nombre_ingresado;
			strncpy ( mayor_nombre, nombre_ingresado, sizeof(mayor_nombre) );
			//mayor_apellido = apellido_ingresado;
			strncpy ( mayor_apellido, apellido_ingresado, sizeof(mayor_apellido) );
			mayor_edad = edad_ingresada;
		}
	}

	edad_promedio = (float) edad_sumatoria / (float) cantidad_personas;

	printf("\nRESUMEN\n");
	printf("=======\n\n");

	printf("Promedio de edad : %f\n", edad_promedio);
	printf("Persona más vieja: %s %s,\tedad : %i\n", mayor_nombre, mayor_apellido, mayor_edad);
	printf("Persona más joven: %s %s,\tedad : %i\n", menor_nombre, menor_apellido, menor_edad);

}

int validarTexto(char *texto)
{
	int i;
	char c;
	int salida;

	salida = 1;

	for(i=0; i<TAMANO_TEXTO; i++)
	{
		c = texto[i];

		// eliminar linea
		// revisar cada char para validar
		//printf("\tpos: %i > char: %c > int: %i\n", i, c, texto[i]);

		if(i==0 && c==0)
		{
			salida = 0;
			break;
		}

		//else if(c==0)
		else if(c==0 || texto[i]==0)
		{
			break;
		}

		else if(i==0 && !(65<=c && c<=90))
		{
			salida = 0;
			break;
		}

		else if(i>0 && !(97<=c && c<=122))
		{
			salida = 0;
			break;
		}

	}

	return salida;
}

int validarEdad(int edad)
{
	return (EDAD_MIN <= edad && edad <= EDAD_MAX);
}
